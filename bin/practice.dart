void main(List<String> arguments) {
  Shepherd rex = Shepherd();
  Poodle fex = Poodle();
  Bulldog pex = Bulldog();

  sayBreed(rex);
  sayBreed(pex);
  sayBreed(fex);
  pex.makeNoise();

  var kitty1 = Cat('kyzya1');
  var kitty2 = Cat('kyzya1');
  var kitty3 = Cat('kyzya3');

  kitty1.log();
  kitty2.log();
  kitty3.log();
}

abstract class DogBreed {
  String breedName;
}

void sayBreed(DogBreed breed) {
  print('My breed is ${breed.breedName}');
}

mixin BarkingDogMixin {
  void makeNoise() {
    print('Gav-gav');
  }
}

class Shepherd implements DogBreed {
  String breedName = "Shepherd";
}

class Poodle implements DogBreed {
  String breedName = "Poodle";
}

class Bulldog with BarkingDogMixin implements DogBreed {
  String breedName = "Bulldog";
}

class Cat {
  String name;
  bool mute = false;

  static final Map<String, Cat> _cache = <String, Cat> {};

  factory Cat(String name) {
    return _cache.putIfAbsent(name, () => Cat._internal(name));
  }

  Cat._internal(this.name){
    print('name ${name} was added to cash, and constructor was called');
  }

  void log () {
    print ('My name is ${name}');
  }
}